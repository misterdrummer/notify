import UIKit
import MapKit
import CoreLocation

struct GeoKey {
  static let latitude = "latitude"
  static let longitude = "longitude"
  static let radius = "radius"
  static let identifier = "identifier"
  static let name = "name"
  
}



class Geotification: NSObject, NSCoding, MKAnnotation {
  
    //mise en place des paramètre des location 
  var coordinate: CLLocationCoordinate2D
  var radius: CLLocationDistance
  var identifier: String
  var name: String

  
  var title: String? {
    if name.isEmpty {
      return "No Note"
    }
    return name
  }
  
  
  
  init(coordinate: CLLocationCoordinate2D, radius: CLLocationDistance, identifier: String, name: String) {
    self.coordinate = coordinate
    self.radius = radius
    self.identifier = identifier
    self.name = name
    
  }
  
  // MARK: NSCoding
  required init?(coder decoder: NSCoder) {
    let latitude = decoder.decodeDouble(forKey: GeoKey.latitude)
    let longitude = decoder.decodeDouble(forKey: GeoKey.longitude)
    coordinate = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
    radius = decoder.decodeDouble(forKey: GeoKey.radius)
    identifier = decoder.decodeObject(forKey: GeoKey.identifier) as! String
    name = decoder.decodeObject(forKey: GeoKey.name) as! String
   
  }
  
  func encode(with coder: NSCoder) {
    coder.encode(coordinate.latitude, forKey: GeoKey.latitude)
    coder.encode(coordinate.longitude, forKey: GeoKey.longitude)
    coder.encode(radius, forKey: GeoKey.radius)
    coder.encode(identifier, forKey: GeoKey.identifier)
    coder.encode(name, forKey: GeoKey.name)
    
  }
  
}
