import UIKit
import MapKit
import CoreLocation

protocol AddViewControllerDelegate {
    func addViewController(controller: AddViewController, didAddCoordinate coordinate: CLLocationCoordinate2D,
                           radius: Double, identifier: String, name: String)
}

class AddViewController: UIViewController, CLLocationManagerDelegate, MKMapViewDelegate {
    
    
    @IBOutlet weak var addButton: UIBarButtonItem!
    @IBOutlet weak var zoomButton: UIBarButtonItem!
    @IBOutlet weak var nameText: UITextField!
    @IBOutlet weak var radiusText: UITextField!
    @IBOutlet weak var mapView: MKMapView!
    
    
    
    
    var delegate: AddViewControllerDelegate?
    var geotifications: [Geotification] = []
    
    let locationManager = CLLocationManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.leftBarButtonItems = [zoomButton]
        navigationItem.rightBarButtonItems = [addButton]
        addButton.isEnabled = false
        
        
        
        locationManager.delegate = self;
        locationManager.distanceFilter = kCLLocationAccuracyNearestTenMeters;
        locationManager.desiredAccuracy = kCLLocationAccuracyBest;
        
        
        mapView.delegate = self
        mapView.showsUserLocation = true
        mapView.userTrackingMode = .follow
        
    }
    
    
    func updateGeotificationsCount ( )  {
        
        navigationItem.rightBarButtonItem? .isEnabled =  ( geotifications.count < 20 )
    }
    
    
    
    
    @IBAction func changedText(_ sender: UITextField) {
        addButton.isEnabled = !radiusText.text!.isEmpty && !nameText.text!.isEmpty
    }
    
    
    @IBAction func dismissKeyboard(_ sender: AnyObject) {
        
        self.resignFirstResponder()
    }
    
    
    @IBAction func dismissKey(_ sender: AnyObject) {
        
        self.resignFirstResponder()
    }
    
    
    @IBAction private func onAdd(_ sender: AnyObject) {
        let coordinate = mapView.centerCoordinate
        let radius = Double(radiusText.text!) ?? 0
        let identifier = NSUUID().uuidString
        let name = nameText.text
        delegate?.addViewController(controller: self, didAddCoordinate: coordinate, radius: radius, identifier: identifier, name: name!)
    }
    
    
    
    @IBAction private func zoomLocation(_ sender: Any) {
        mapView.zoomToUserLocation()
    }
    
    
    
    
    
}
